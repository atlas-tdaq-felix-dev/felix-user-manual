# Update this manual


```
gem install asciidoctor asciidoctor-pdf
```

```
npm install --global @antora/cli @antora/site-generator @antora/site-generator-default git-describe
npm install @antora/lunr-extension tar-stream fs-extra
```

- ./make-pdf.sh will run asciidoctor-pdf and generate the pdf file.
- ./make-html will re-generate the site, including the help files and the (already updated) pdf.

Known Problems
- warnings in asciidoc may not show in antora
- errors in asciidoc do show, but antora does not fail
- titles mention - Untitled
- table counter reset does NOT work in PDF

public: https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/felix-doc/index.html
staging: https://atlas-project-felix.web.cern.ch/atlas-project-felix/dev/staging/www/user/felix-doc/index.html

# On First Clone

This repository uses Submodules. If you just cloned this repository you need to check out the submodules.

```
git submodule update --init --recursive
```

Mark Donszelmann
