function inject_toc() {
    var fwin = window.frames["toc"].window;
    if (fwin) {
        fwin.eval(tocroutine.toString());
        fwin.eval("tocroutine();");
    }
}

function tocroutine() {
    var footer = document.getElementById("footer");
    if (footer)
        footer.remove();

    var url = window.location.href + "/";
    url = url.substring(0, url.indexOf("/", 8));

    var links = document.getElementsByTagName("a");
    for (var i = 0; i < links.length; i++) {
        links[i].onclick = parent.toc_nav;
        links[i].href_dat = links[i].href.substring(url.length + 1);
        links[i].href_local = "#_" + i;
        links[i].id = "_" + i;
    }

    var anchor = (window.location.href.split('#').length > 1) ? window.location.href.split('#')[1] : null;
    if (anchor != null && anchor != "") {
        var elem = document.getElementById(anchor);
        elem.scrollIntoView();
    }
}

function toc_nav(e) {
    e.preventDefault();

    update_nav(this.href_dat, undefined);

    var url = window.location.href;
    url = url.substring(0, url.indexOf("?"));

    iframe_nav("toc", url + S_GET("ver") + "/toc.html" + this.href_local);
}

function inject_main() {
    var fwin = window.frames["main"].window;
    if (fwin) {
        fwin.eval(main_inject_routine.toString());
        fwin.eval("main_inject_routine();");
    }
}

function main_inject_routine() {
    var links = document.getElementsByTagName("a");

    var url = window.location.href + "/";
    url = url.substring(0, url.lastIndexOf("/", url.length - 2));

    for (var i = 0; i < links.length; i++) {
        if (links[i].target == "_blank") continue;

        links[i].onclick = parent.main_nav;
        links[i].href_dat = links[i].href.substring(url.length + 1);
    }
}

function main_nav(_, no_url) {
    if (no_url) {
        update_nav(_.href_dat, undefined, true);
    } else {
        _.preventDefault();
        update_nav(this.href_dat, undefined);
    }
}

function changeVersion() {
    var ver = document.getElementById("version-select").value;

    update_nav(undefined, ver);
}

function update_nav(nav, ver, force) {
    var _nav = nav || S_GET("nav");
    var _ver = ver || S_GET("ver");

    history.pushState(null, "test", "?ver=" + _ver + "&nav=" + encodeURIComponent(_nav));
    state_changed(force);
}

function S_GET(id) {
    try {
        var a = new RegExp(id+"=([^&#=]*)");
        return decodeURIComponent(a.exec(window.location.search)[1]);
    } catch (exception) {
        return "";
    }
}

function search(query) {
    if (query == "") {
        update_nav(undefined, undefined, true);
        return;
    }

    var res1 = search_agent.search(query, { "expand": true });

    var fdoc = window.frames["main"].document;
    var body = fdoc.getElementsByTagName("body")[0];
    while (body.firstChild)
        body.removeChild(body.firstChild);

    var head_div = document.createElement("div");
    head_div.id = "header";
    var header = document.createElement("h1");
    header.textContent = "Search results";
    head_div.appendChild(header);
    body.appendChild(head_div);

    var content_div = document.createElement("div");
    content_div.id = "content";
    res1.forEach(function(item) {
        var itm = item.doc;

        var elem_div = document.createElement("div");
        elem_div.className = "sect2";

        var elem_head = document.createElement("h3");
        elem_head.textContent = itm.title;
        elem_head.style = "margin-bottom: 0;"
        elem_div.appendChild(elem_head);

        var elem_content = document.createElement("div");
        elem_content.className = "paragraph";
        var p = document.createElement("p");
        var a = document.createElement("a");
        a.textContent = "View";
        a.href = "#";
        a.onclick = function() { parent.main_nav(this, true) };
        a.href_dat = itm.filename.substring(1);
        p.appendChild(a);
        p.appendChild(document.createElement("br"));
        p.append(itm.body.substring(0, itm.body.indexOf("\n\n")));
        elem_content.appendChild(p);

        elem_div.appendChild(elem_content);

        content_div.appendChild(elem_div);

    });
    body.appendChild(content_div);
}

var last_ver = "";
var last_nav = "";
function state_changed(force) {
    var url = window.location.href;
    url = url.substring(0, url.indexOf("?"));

    var _ver = S_GET("ver");
    var _nav = S_GET("nav");
    if (_nav != "") {
        iframe_nav("main", url + _ver + "/" + _nav);
        if (!window.chrome) {
            setTimeout(function() {
                if (force && _nav.substring(0, _nav.indexOf("#")) == last_nav.substring(0, last_nav.indexOf("#")))
                    window.frames["main"].location.reload(true);
            }, 500);
        }
        last_nav = _nav;
    }

    if (_ver != last_ver) {
        var selector = document.getElementById("version-select");
        while (selector.firstChild)
            selector.removeChild(selector.firstChild);
        versions.forEach(function(version) {
            var opt = document.createElement("option");
            opt.value = version.dir;
            opt.innerHTML = version.version;
            selector.appendChild(opt);
        });
    }

    if (_ver != "" && _ver != last_ver) {
        selector.value = _ver;
        iframe_nav("toc", url + _ver + "/toc.html");

        if (last_ver != "" || _nav == "") { // only return to index when coming from different version or when no page is selected
            iframe_nav("main", url + _ver + "/index.html");
            if (force)
                window.frames["main"].location.reload(true);
        }

        index_search();
        last_ver = _ver;
    } else if (_ver == "") {
        update_nav("", versions[0].dir);
    }
}
window.onpopstate = state_changed;

function iframe_nav(name, url) {
    var elem = document.getElementsByName(name)[0];
    var parent = elem.parentNode;

    parent.removeChild(elem);
    elem.setAttribute("src", url);
    parent.appendChild(elem);
}

var versions;
const use_dyn_ver = DYN_VER;
var req = new XMLHttpRequest();
req.open("GET", "versions.json" + (use_dyn_ver ? ".php" : ""), false);
req.onreadystatechange = function() {
    if (req.readyState == 4 && req.status == 200) {
        versions = JSON.parse(req.responseText);
    }
};
req.send();

var nav_elements = [];
function index_search() {
    search_agent = elasticlunr();
    search_agent.addField("title");
    search_agent.addField("body");

    req = new XMLHttpRequest();
    req.open("GET", S_GET("ver") + "/search_index.json", true);
    req.onreadystatechange = function() {
        if (req.readyState == 4 && req.status == 200) {
            var search = JSON.parse(req.responseText);

            last_name = "///////////////"; // a string that can't accidentally be contained in the url
            search.forEach(function (doc) {
                search_agent.addDoc(doc);

                name = doc.filename.substring(0, doc.filename.indexOf("#"));
                if (!doc.filename.startsWith(last_name))
                    nav_elements.push(name);
                last_name = name
            });
        }
    };
    req.send();
}

function nav_lr(step) {
    idx = S_GET("nav") == "" ? 0 : false;

    if (idx === false) {
        for (var i = 0; i < nav_elements.length; i++) {
            if (S_GET("nav").startsWith(nav_elements[i].substring(1))) {
                idx = i;
                break;
            }
        }
        if (idx + step >= 0 && idx + step < nav_elements.length)
            idx += step;
        else return;
    }

    update_nav(nav_elements[idx].substring(1), undefined, true);
}
