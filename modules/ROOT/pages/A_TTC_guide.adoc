<<<
ifeval::["{backend}" == "html5"]
:nofooter:
:docinfo: shared-footer
:leveloffset: +1
endif::[]
:xrefstyle: short
:doc-part: A
ifeval::["{backend}" == "pdf"]
:!figure:
{counter:figure:0}
:!table:
{counter:table:0}
endif::[]
:numbering:
[#app:TTC]
= Setting up a TTC System for use with FELIX

This section is meant to help users of FELIX systems with the set-up of a TTC system. Both the new ALTI and legacy TTCvx/TTCvi systems are described below.

NOTE: When connecting a TTC system to the FLX-712 card, note that the hardware is sensitive to signals down to -36 dBm and up to -3 dBm. Incoming optical power should be within this range.

== The ALTI System

The ATLAS Local Trigger Interface (ALTI) is an upgrade to the former TTC system, and replaces the functionality of the previous LTPi, LTP, TTCVi and TTCvx. The ALTI board provides functionalities such as rate counters for all TTC signals, per-bcid counters, busy monitoring, a pattern generation, and as monitoring/synchronization of input signals and programable phase shift of output signals. For a full list of functionalities and detailed ALTI instructions please see:

link:https://twiki.cern.ch/twiki/bin/viewauth/Atlas/LevelOneCentralTriggerALTI[https://twiki.cern.ch/twiki/bin/viewauth/Atlas/LevelOneCentralTriggerALTI]

The forward signals are ones sent by the CTP to the sub-systems (BC, ORB, L1A, TTR, BGO, TTYP), while the backward signals are sent by the subsystems to the CTP (BUSY, calibration). The front panel of the ALTI also has six SFP connectors for -mode fibre optic transmitter/receiver modules. The first five SFPs are used for dual-transmitters, while the last one can be used for TTC signal monitoring.

To test the FELIX with the ALTI card, connect system as shown in <<#fig:alti>>. The cable from the LEMO connector of the FELIX timing card is plugged into the lower left connector labelled as BUSY OUT. In the future, it will be possible to configure the BUSY connector on the ATLI for either a NIM or TTL signal via software. Until this software feature is available in the ALTI, a  NIM to TTL adapter can be used since the FELIX sends a signal compatible with a TTL signal, while the ALTI accepts a NIM signal by default.

To send TTC signals (L1A for example) from the ALTI to the FELIX, connect top SFP connector (using a single LC connector), to the ST connector on the FELIX timing card as shown in <<#fig:alti>>. An LC to ST adapter will be needed for this.

The bottom SFP connector with the orange connections is connected in loopback mode for ALTI debugging and can be ignored for the purposes of FELIX commissioning.

[#fig:alti,width=50%]
.Image of cabled ALTI
image::figures/ALTI.png[caption="Figure {doc-part}.{counter:figure} "]

=== Software Setup

Once the hardware is setup log into the SBC hosting the crate and setup the TDAQ and ALTI software either locally, in testbed, or at Point&#8201;1 following instructions at:

link:https://twiki.cern.ch/twiki/bin/viewauth/Atlas/LevelOneCentralTriggerALTI#Software[https://twiki.cern.ch/twiki/bin/viewauth/Atlas/LevelOneCentralTriggerALTI#Software]

Once the TDAQ and ALTI environment is setup, configure the correct slot for the ALTI using the command:

`testAltiInitial -s 6 -B -b 0x07000000 -R -S -c`

where -s 6 is the ALTI slot number, -B -b 0x07000000 is to change the base address to 0x07000000 and -R -S -c is to reset and check the ALTI board.

=== Sending TTC Signals with ALTI

First make sure the FELIX is set to TTC clock following instructions in <<3_hardware_setup.adoc#subsubsec:clocksel>> and <<3_hardware_setup.adoc#subsubsec:clockRecovery>>.
After the software is setup you are ready to test sending L1A with the ATLI. You can use a configuration located here to send the L1A from the ATLI: /afs/cern.ch/atlas/project/tdaq/level1/ctp/l1ct-08-03-05/ALTI/data/cfg_1MHz.dat.

The file sends Level-1 Accepts (L1A) at a 1 MHz rate. You can either copy the file locally or read it directly from afs if you have access to it from your TTC crate. You can then start sending the L1A by executing the command below from the SBC:

`testAltiInitial -s 4 -R -S -C -f  /afs/cern.ch/atlas/project/tdaq/level1/ctp/l1ct-08-03-05/ALTI/data/cfg_1MHz.dat`

At this point the FELIX should forward the L1A to the front end, your front end should respond accordingly and send data through the FELIX, which you can monitor with FELIXcore.

If one would like to stop/start the pattern sending you can follow the commands below from the SBC, and selecting 7, followed by 3 (to enable L1A sending) or 4 (to disable L1A sending):

`menuAltiModule`

`  7  [PAT menu] Pattern generation memory`

`     3  enable    pattern generation`

`     4  disable   pattern generation`

If a different patter generation or a different frequency of pattern is required, it is possible to configure one following instructions on

link:https://twiki.cern.ch/twiki/bin/viewauth/Atlas/LevelOneCentralTriggerALTI#Scripts_to_generate_of_Pattern_G[https://twiki.cern.ch/twiki/bin/viewauth/Atlas/LevelOneCentralTriggerALTI#Scripts_to_generate_of_Pattern_G]


=== Testing BUSY signal with ALTI

In order to test whether the ALTI is corresponding correctly to a BUSY from the FELIX, it is possible to force a BUSY in the FELIX using the commands below on the FELIX server:


`flx-config set TTC_DEC_CTRL_BUSY_OUTPUT_INHIBIT=0 -d 2`

`flx-config set TTC_DEC_CTRL_MASTER_BUSY=1  -d 2`


To remove the BUSY execute the commands below on the FELIX card:

`flx-config set TTC_DEC_CTRL_BUSY_OUTPUT_INHIBIT=0 -d 2`

`flxcard $ ./flx-config set TTC_DEC_CTRL_MASTER_BUSY=0  -d 2`

The ALTI should respond to the FELIX BUSY by stopping to send the L1A (or other generated) patterns. To test if the ALTI has stopped sending the patterns, the following can be executed on the SBC hosting the ALTI:

`menuAltiModule`

And select 11 CNT menu counters, and then select 1 to read counters. If selecting 1 to read counters several times does not make the counters go up, it means the ALTI has stopped sending the L1A (or other pattern), and thus correctly responded to the FELIX BUSY signal.



== The TTCvi/TTCvx (A)

<<#fig:final_ttc_cabling>> shows the final cabling of TTCvi and TTCvx modules for a TTC setup with B-channel.
The A-channel carries the Level-1 Accept; the B-channel carries BCR and the other TTC commands.
The TTCvi-TTCvx pair should have already been tuned. If not, see <<sec:TTCtuning>> below.
Note: For a TTCex this may look different.
A list of all the materials you will require to set up a TTC system is presented in <<tab:ttc>>.


[#fig:final_ttc_cabling]
.Image of cabled TTC system with B-channel connections
image::figures/TTC_CableSetup.png[caption="Figure {doc-part}.{counter:figure} "]

[[tab:ttc]]
[cols=3*, caption="Table {doc-part}.{counter:table} "]
.Materials needed to set up a TTC system
|===
|Item | Source | Remarks
|VMEbus crate | Can be rented from the CERN Electronics Pool | Other crates may do as well
|VMEbus master | We recommend a SBC from Concurrent Technologies (ATLAS standard). Support can be given for VP717, VP917 and VP-E24 (64 bit, compatible with TDAQ software release tdaq-05-05-00 and above).
|TTCvi VMEbus card | Can be rented from the CERN Electronics Pool (but the Pool may be out of stock) | The TTCvi is no longer in production. Make sure the VME base address switches are set to match your software.
|TTCvx VMEbus card or TTCex VMEbus card | TTCvx and TTCex can be rented from the CERN Electronics Pool (but the Pool may be out of stock) | The TTCvx/ex is no longer in production. The TTCvx has a LED driver, the TTCex has a laser driver
|3 LEMO cables (1 or 0.5 ns) | |
|1 optical multi-mode (TTCvx) or single-mode (TTCex) fiber with ST connectors on both ends | | Max length of the fibre: TTCvx: 20 m; TTCex: 100 m
|TTCoc & ATLAS (not clear who to ask; Maybe P. Farthouat) & TTC fan-out; needed if you have several FELIX
|optical attenuator | | Needed only for use with a TTCex without TTCoc. The optical attenuator has to be a single-mode attenuator of 3-20 dB and has to be connected directly to the TTCex output. The FTPDA-R155 should work with a TTCvx without attenuator. In case of a TTCex an attenuator of 3 dB is recommended for the FTPDA-R155. The FTPDA-R155 has a sensitivity of -31 dBm and saturates at +1 dBm.
3+a| If you need to tune the TTCvi-TTCvx pair, you need in addition:

* 2 LEMO cables (5-10 ns)
* 2 LEMO Y-adapters
* 2 LEMO-BNC adapters
* 2 50 Ohm terminators (Only required if your oscilloscope has no internal termination.)
|===

[[sec:TTCtuning]]
=== Tuning a TTC system

If your TTCvi-TTCvx pair has not been tuned, follow the instructions in this section.
Cable the TTC system as shown in <<fig:ttc_cabling>>. Note: for a TTCex this may look different.
For more information please consult the section "Tuning procedure 2" of the TTCvi manual (http://www.cern.ch/TTC/TTCviSpec.pdf).

[#fig:ttc_cabling]
.Image of cabling for tuning a TTC system
image::figures/ttc_cabling.png[caption="Figure {doc-part}.{counter:figure} "]

Note: The question has come up if channel A and channel B are correctly cabled in the picture
above. Here is a reply from the TTC expert (Sophie Baron):

A "good" configuration when channel B is not used is indeed to have it tied to "1". And it is
right that having Channel B connected to OUTPUT B gives a static "1" on channel B. However, the
termination scheme at the TTCex inputs keeps as well unconnected channel inputs (both B and A) to
"1" by default (it is negative ECL logic, and the Vin is at -2.08V by default). Therefore, both
schemes could be used identically. One additional remark: of course, if you leave both A and B
unconnected at the input of the TTCex, you will have both channels A and B to "1", and this is
not good as the TTCrx needs to see two different behaviours on A and B to be able to differentiate
them (the rule is that the A-channel must not have more than 11 consecutive "1" whereas B can have
any type of sequence).

This description can be broken down into the following points:

* Connect the TTCvi *A/ecl CHANNEL OUT* output to the TTCvx *A/ecl CHANNEL IN* input via a Y-adapter.

[#fig:ttc_delayset]
.Image of cabling for tuning a TTC system
image::figures/ttc_delayset.png[caption="Figure {doc-part}.{counter:figure} "]

* Connect, via a Y-adapter, one of the *TTCvx CLOCK OUT/ecl* outputs to the *TTCvi CLOCK IN bc/ecl* input.
Check that the BC_EXT indicator is lit on the TTCvi as shown below. The TTCvx internal clock may be used.

* Set the TTCvi trigger mode (= 5) to random at the highest rate (100 kHz) and disable the event/orbit/trigger-type
transfers. In order to do this write 0x7005 to the D16 VMEbus CSR1 register at offset 0x80. This can be done easily
for vme_rcc_test. Note: The A24 base address of the TTCvi in the CERN reference system in TBED is 0x555500. This should
light up the TTCvi A-Ch yellow indicator and the A/ecl CHANNEL OUT output should now carry 25 ns long trigger pulses.

* With an oscilloscope look at the TTCvx Channel-A input in respect to the clock output, as shown below.

[#fig:ttc_scope]
.Image of cabling for tuning a TTC system
image::figures/ttc_scope.png[caption="Figure {doc-part}.{counter:figure} "]

* Adjust the TTCvi BC delay switch such that the rising edges of the Channel-A pulses occur within 4 ns before to 2 ns
after the rising edges of the clock signal.

* Setting the delay switch in position 2 and using 1 ns long interconnecting cables for the clock and the A and B channels
corresponds to the above mentioned timing criteria. Note from Markus Joos: Even though I used 1 ns cables, I had to set the
switch to position 5 (see picture above) in order to meet the requirement of step 5.


=== Guide to TTC Channel B

The following section describes the structure of the TTC 'B channel' data stream, and how it may be decoded and operated by users.
The information in this section is provided courtesy of Alessandra Camplani and the LAr group.

The data stream arriving through TTC B channel can be of two types: short broadcast commands or long individually-addressed commands/data.

Short broadcast commands are used to deliver messages to all TTC destinations in the system, while long individually-addressed commands/data
are used to transmit user-defined data and instructions over the network to specific addresses and sub-addresses. These two types of command
have different dedicated frame formats, as shown in <<fig:frame>>:

[#fig:frame,width=50%]
.Image of cabling for tuning a TTC system
image::figures/bchanframe.png[caption="Figure {doc-part}.{counter:figure} "]

The difference between the two command types can be illustrated with the example below. When not in use the B channel IDLE state is set to 1.
When a sequence of commands is sent, the data transmission state changes from 1 to 0. After the first zero received it is possible to distinguish
between short broadcast and long address commands: if the second bit in the stream is a 0 then the command is a short broadcast, if it is a 1 then
the command is of long address type.
[subs="verbatim"]
----
IDLE=111111111111
    Short Broadcast, 15 bits:
    00TTDDDDEBHHHHH:
        T= test command, 2 bits
        D= Command/Data, 4 bits
        E= Event Counter Reset, 1 bit
        B= Bunch Counter Reset, 1 bit
        H= Hamming Code, 5 bits
    Long Addressed, 41 bits
    01AAAAAAAAAAAAAAE1SSSSSSSSDDDDDDDDHHHHHHH:
        A= TTCrx address, 14 bits
        E= internal(0)/External(1), 1 bit
        S= SubAddress, 8 bits
        D= Data, 8 bits
        H= Hamming Code, 7 bits
----

The short broadcast command type is used to send two important values: the Bunch Counter
Reset (BCR) and the Event Counter Reset (ECR).

The BCR is used to reset the bunch crossing counter, which is increased every clock cycle
on the 40 MHz clock. This is a 12-bit counter, also called BCID. A BCR command is sent
roughly every 89 &#956;s, corresponding to the time that a bunch needs to do an entire
circuit of the LHC. During this time the BCID counter reach its maximum value, 3564 counts.

The ECR is used to increase the event reset counter. The periodicity of this reset is decided
by each experiment, with ATLAS having it set to 5 seconds. The event reset counter combined
with the L1A counter gives the Extended L1ID (EVID). This is a 32-bit value consisting the
L1A counter in the lower 24 bits, and the event reset counter in the upper 8. Every time that
an ECR is received the upper counter is increased by 1 and the lower part is reset to zero.
Every time that a L1A is received the lower part is increased by 1.

BCID and EVID values are used as a label for the data accepted by the trigger.

The long address command type is used to transport another important value: the Trigger Type
(TType). Each L1A transmission is followed, with variable latency, by an 8-bit TType word.
This word is generated inside the LVL1 Central Trigger Processor (CTP) and  distributed from
the CTP to the TTCvi modules for each of the TTC zones in the experiment via the corresponding
LTP modules.

The presence of a Trigger Type within long address commands is announced by a sub-address
(8 bits) set to 0.


[[tab:ttype]]
[cols=9*, caption="Table {doc-part}.{counter:table} "]
.Trigger type 8-bit word: Each bit represent the sub-detector which fired the trigger or the data type.
|===
|	*Sub-Trigger* | physics | ALFA | FTK | LAr demonstrator | Muons | Calorimeter | ZeroBias | Random
|	*Bit* | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0
|===


As shown in <<tab:ttype>>, each bit has a specific role. In calibration mode, bits 0 to 2 can be used to distinguish
between up to eight different possible types of calibration trigger within each sub-detector. Bits 3 to 6 are used to
indicate which sub-detector or subsystem fired the trigger. Bit 7 represents physics trigger-mode when set to 1, and
calibration mode when set to 0.

=== B channel decoding firmware

An effort is under way to provide a centrally maintained firmware module to decode TTC B-channel data. In the short
term, users are advised to refer to a version produced for LAr front-ends by Alessandra Camplani. The module code
can be found in gitlab:

https://gitlab.cern.ch/atlas-lar-ldpb-firmware/LATOME-ttc

The code itself is in the folder _code_ttc_ and the files dedicated to TType decoding
are: _Bchan_top.vhd_, _SMdecoding_cnt.vhd_ and _TType_decoding_.
The simulations for this specific part can be found in the _simulation_ folder. Here there is a testbench for
the  _Bchan_top_ entity and another one for _TType_decoding_ entity.

Development of this module is ongoing, with the _latome_ttc_ branch being actively maintained and kept up-to-date.

=== Channel B decoding software

In order to test channel decoding, it is recommended that users employ the menuRCDTtcvi application, provided as part
of the ATLAS TDAQ software release. Within the application select 'BGO menu' and then option 13 'send asynchronous command'.
From here it should be possible to select either a short of long command. In the case of a short command simply enter the data
word to be sent. For a long command enter an address 0 (for broadcast), 0 for internal registers, subaddress 0 for trigger type,
and the data word to be sent.


=== Useful documents

You may find additional useful information in this document from the ATLAS LAr group:

https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/community/CPPM_MiniFELIX_tests_results_and_TTC_system_experience.pdf
