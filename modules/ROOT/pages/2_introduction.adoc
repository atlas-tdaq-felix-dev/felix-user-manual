<<<
ifeval::["{backend}" == "html5"]
:nofooter:
:docinfo: shared-footer
:leveloffset: +1
endif::[]
:xrefstyle: short
:doc-part: 2
ifeval::["{backend}" == "pdf"]
:!figure:
{counter:figure:0}
:!table:
{counter:table:0}
endif::[]
[#sec:introduction]
:numbering:
= Introduction to FELIX

FELIX is a detector readout component developed as part of the ATLAS upgrade effort.
FELIX is designed to act as a data router, receiving packets from detector front-end electronics
and sending them to programmable peers on a commodity high bandwidth network. Whereas previous detector
readout implementations relied on diverse custom hardware platforms, the idea behind FELIX is to
unify all readout across one well supported and flexible platform. Rather than the previous hardware
implementations, detector data processing will instead be implemented in software hosted by commodity
server systems subscribed to FELIX data. From a network perspective FELIX is designed to be flexible
enough to support multiple technologies, including TCP/IP and RoCE.

The FELIX readout system is based on a custom PCIe "FELIX" card hosted on a commodity server.
Front-end data is transferred from the FELIX card into the host memory into a so-called
direct-memory-access (DMA) buffer. The host forwards the data to remote clients over
a commodity network. Data can also flow in the opposite direction, from a remote client to
a front-end.

== FELIX Variants and Functionality

FELIX supports different link protocols for the transfer of data to and from front-end peers.
Each is supported by the same hardware platform, with separate firmware revisions both based on
the same core modules.
A complete description of all the supported protocols and firmware flavours can be found in the
https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/docs/FELIX_Phase2_firmware_specs.pdf[Phase-II FELIX firmware specification document]

---

=== Gigabit Transceiver (GBT) and the Versatile Link

The Gigabit Transceiver (GBT) chipset and associated technologies were developed
as part of https://ep-ese.web.cern.ch/content/gbt-versatile-link[CERN's Radiation Hard Optical Link Project].
GBT provides an interface an optical connectivity technology known as the
http://stacks.iop.org/1748-0221/7/i=01/a=C01075[Versatile link], which provides
a radiation hard transport of data between GBT end points.
The GBT transmission protocol is designed to aggregate multiple lower bandwidth
links from front-end electronics components into one radiation hard high
bandwidth data link (running at up to 5 Gb/s).
The logical lower bandwidth links which make up a GBT link are known as E-links.
The GBT protocol has been implemented both in the dedidated
https://cds.cern.ch/record/2809057[GBTX ASIC] as well as
https://twiki.cern.ch/twiki/pub/Atlas/GBT2LAN/FELIX_GBT_MANUAL.pdf[directly on FPGA platforms],
the latter of which has been built on for use by the FELIX project.

---

=== Low Power Gigabit Transceiver (lpGBT)

https://lpgbt-fpga.web.cern.ch/doc/html/[LpGBT] is the evolution of the GBTX ASIC.
LpGBT supports 2.56 Gb/s downlinks (from the readout system to the front-end)
and 5.12 or 10.24 Gb/s uplinks (from the front-end to the readout system).
Similarly to the GBT protocol, LpGBT also defines E-links.

---

=== FULL Mode

Within the context of the Phase-I ATLAS upgrade a requirement arose for a higher
bandwidth data link from detector to FELIX than was possible with GBT.
These newer clients did not require radiation hardness, and were able to support
a protocol which could be implemented in FPGAs on both sides of the link.
The resulting development is known as 'FULL mode', referring to full link bandwidth.

The FULL mode protocol is a implemented as a single wide data stream with no
handshaking or logical substructure (i.e. no E-links). The reduced constraint
mean that FULL mode links can operate at a line transmission rate of 9.6 Gb/s,
which accounting for 8b10b encoding means a maximum user payload of 7.68 Gb/s.

In the downstream direction FULL mode relays clocks and messages from the Local
Trigger interface (LTI). LTI also operates at 9.6 Gb/s and is 8b10b encoded.
The 40.079 MHz LHC BC clock is recovered from the link 240.474 MHz clock.

The Phase-I (rm-4) version of FULL-mode implemented GBT downlinks that
transmitted the legacy TTC signal using a custom encoding. 

---

=== Interlaken Mode

FELIX supports the Interlaken protocol <<bibliography#fullmodespec,[6]>> for
transmission rates up to 25 Gb/s from the detector.
From a functional point of view, Interlaken is similar to FULL Mode as links
do not have a logical substructure. Downlinks are rated at 9.6 Gb/s and relay
LTI messages.

---

=== ATLAS ITk Pixel and Strip

The ATLAS Phase-II Inner Tracker (ITk) adopted various data encodings,
including Aurora and Endeavour. Dedicated firmware have been developed to
support ITk pixel and strips sub-detectors.

